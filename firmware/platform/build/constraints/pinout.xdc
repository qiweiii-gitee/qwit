# clock
set_property PACKAGE_PIN U18 [get_ports CLK]

set_property IOSTANDARD LVCMOS33 [get_ports CLK]

# iis
set_property PACKAGE_PIN W18 [get_ports IIS_SCLK]
set_property PACKAGE_PIN P14 [get_ports IIS_LRCK]
set_property PACKAGE_PIN Y16 [get_ports IIS_DATA]

set_property IOSTANDARD LVCMOS33 [get_ports IIS_SCLK]
set_property IOSTANDARD LVCMOS33 [get_ports IIS_LRCK]
set_property IOSTANDARD LVCMOS33 [get_ports IIS_DATA]

