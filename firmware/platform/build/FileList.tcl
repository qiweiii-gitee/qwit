#add constraints
add_files -fileset constrs_1 -norecurse ./constraints/pinout.xdc
add_files -fileset constrs_1 -norecurse ./constraints/timing.xdc

#add source
add_files ../source/platform.sv

add_files ../../shared/interfaces/if_reg_ctrl.sv
add_files ../../shared/utils/
add_file  ../../shared/audio/
