#******************************************************************************
#    # #              Name   : Run.tcl
#  #     #            Date   : Jul. 15, 2021
# #    #  #  #     #  Author : Qiwei Wu
#  #     #  # #  # #  Version: 2.0
#    # #  #    #   #
#
# This module is the tcl script of building project.
#
# Change History:
#  VER.   Author         DATE              Change Description
#  1.0    Qiwei Wu       Jul. 15, 2021     Initial Release
#******************************************************************************

proc RunFsbl { buildName } {
   set proc ps7_cortexa9_0
   set os standalone
   sdk set_workspace ./sw_workspace

   sdk create_hw_project -name $buildName\_hw -hwspec $buildName.hdf

   sdk create_app_project -name $buildName\_fsbl -hwproject $buildName\_hw -proc ps7_cortexa9_0 -os standalone -lang C -app {Zynq FSBL}

   # Enable runing the ps7_post_config
   # Set the replace list
   set strOld "if(BitstreamFlag)"
   set strNew "/* if(BitstreamFlag) */"
   set strList [list $strOld $strNew]

   # Read necessary file
   set readFile [open ./sw_workspace/$buildName\_fsbl/src/main.c r]
   set strFile [read -nonewline $readFile]
   close $readFile

   # Replace the string and then rewrite the file
   set strFileNew [string map $strList $strFile]
   set writeFile [open ./sw_workspace/$buildName\_fsbl/src/main.c w]
   puts $writeFile $strFileNew
   close $writeFile

   sdk build_project -type all

   # exit
   exit
}

proc RunFsbl2020 { buildName } {
   # Set Vitis workspace
   setws ./sw_workspace

   # Create application project
   app create -name $buildName\_fsbl -hw $buildName.xsa -os standalone -proc ps7_cortexa9_0 -template {Zynq FSBL}
   app config -name $buildName\_fsbl define-compiler-symbols {FSBL_DEBUG_INFO}
   app build -name $buildName\_fsbl

   # exit
   exit
}

proc RunStandalone { buildName } {
   # set workspace
   sdk set_workspace ./sw_workspace
   sdk create_hw_project -name $buildName\_hw -hwspec $buildName.hdf
   sdk create_bsp_project -name $buildName\_bsp -hwproject $buildName\_hw -proc $proc -os standalone
   sdk create_app_project -name $buildName\_sw -hwproject $buildName\_hw -proc ps7_cortexa9_0 -os standalone -lang C -bsp $buildName\_bsp -app {Empty Application}

   # copy files
   eval file copy -force [glob ../source/*] ./sw_workspace/$buildName\_sw/src/
   eval file copy -force [glob ../../../code/software/standalone/*] ./sw_workspace/$buildName\_sw/src/
   eval file copy -force [glob ../../../code/software/utils/*] ./sw_workspace/$buildName\_sw/src/applications/

   # build
   sdk build_project -type bsp -name $buildName\_bsp
   sdk build_project -type app -name $buildName\_sw

   # exit
   exit
}

proc RunStandalone2020 { buildName } {
   # Set Vitis workspace
   setws ./sw_workspace

   # Create application project
   app create -name $buildName\_sw -hw $buildName.xsa -proc ps7_cortexa9_0 -os standalone -lang C -template {Empty Application}

   # copy files
   eval file copy -force [glob ../source/*] ./sw_workspace/$buildName\_sw/src/
   eval file copy -force [glob ../../../code/software/standalone/*] ./sw_workspace/$buildName\_sw/src/
   eval file copy -force [glob ../../../code/software/utils/*] ./sw_workspace/$buildName\_sw/src/applications/

   # build
   app build -name $buildName\_sw

   # exit
   exit
}

# RunFsbl qwi00_led
