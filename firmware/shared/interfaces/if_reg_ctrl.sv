
//****************************************************************************
//if_reg_ctrl.sv
//****************************************************************************
interface if_reg_ctrl
#(
   ADDR_WID = 13
);

   logic [ADDR_WID-1:0] addr;
   logic [31:0]         wdata;
   logic [31:0]         rdata;
   logic                en;
   logic [3:0]          we;
   
   modport master
   (
      output addr,
      output wdata,
      input  rdata,
      output en,
      output we
   );
   
   modport slave
   (
      input  addr,
      input  wdata,
      output rdata,
      input  en,
      input  we
   );
   
endinterface
