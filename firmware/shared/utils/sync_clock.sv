
module sync_clock
#(
   parameter             DWID = 32
)
(
   input      [DWID-1:0] idata,
   input                 oclk,
   output     [DWID-1:0] odata
);
   
   reg [DWID-1:0] idata_r1;
   reg [DWID-1:0] idata_r2;
   reg [DWID-1:0] idata_r3;
   
   always @(posedge oclk)
   begin
      idata_r1 <= idata;
      idata_r2 <= idata_r1;
      if(idata_r2 == idata_r1)
         idata_r3 <= idata_r2;
   end
   
   assign odata = idata_r3;
   
endmodule
