//*****************************************************************************
//    # #              Name   : hard_reg
//  #     #            Date   : Jul. 10, 2021
// #    #  #  #     #  Author : Qiwei Wu
//  #     #  # #  # #  Version: 1.0
//    # #  #    #   #
// hard_reg.sv
//
// Change History:
//  VER.   Author         DATE              Change Description
//  1.0    Qiwei Wu       Jul. 10, 2021     Initial Release
//*****************************************************************************

module hard_reg
(
   input                       reset,
   input                       reg_clk,
   if_reg_ctrl.slave           reg_if,
   output reg [3:0]            led
);
   
//*****************************************************************************
// Reg contrl
//*****************************************************************************
   reg [31:0] fw_ver;
   reg [31:0] test_reg;
   
always @(posedge reg_clk)
begin
   if(reset)
   begin
      fw_ver <= 32'h00010000;
      test_reg <= 32'h12345678;
      led <= 4'hF;
   end
   else if(&reg_if.we && reg_if.en)
   begin
      case(reg_if.addr)
         8'h01 : test_reg <= reg_if.wdata[31:0];
         8'h02 : led <= reg_if.wdata[3:0];
      endcase
   end
   else if(reg_if.en)
   begin
      case(reg_if.addr)
         8'h00 : reg_if.rdata = fw_ver;
         8'h01 : reg_if.rdata = test_reg;
         8'h02 : reg_if.rdata = led;
         default : reg_if.rdata = 32'hdeadbeef;
      endcase
   end
end
   
endmodule