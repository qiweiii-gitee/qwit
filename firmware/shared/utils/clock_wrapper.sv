
module clock_wrapper
(
   input                       clk_50m,
   output                      clk_3p072m
);
   
   wire clk_6p144m_pre;
   wire clk_6p144m;
   wire clk_fb_pre;
   wire clk_fb;
   reg  clk_3p072m_pre = 0;
   
   MMCME2_BASE
   #(
      .BANDWIDTH               ( "OPTIMIZED" ),   // Jitter programming (OPTIMIZED, HIGH, LOW)
      .CLKFBOUT_MULT_F         ( 44.375 ),     // Multiply value for all CLKOUT (2.000-64.000).
      .CLKFBOUT_PHASE          ( 0.0 ),      // Phase offset in degrees of CLKFB (-360.000-360.000).
      .CLKIN1_PERIOD           ( 20.0 ),       // Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
      .CLKOUT0_DIVIDE_F        ( 120.375 ),    // Divide amount for CLKOUT0 (1.000-128.000).
      .CLKOUT0_DUTY_CYCLE      ( 0.5 ),
      .CLKOUT0_PHASE           ( 0.0 ),
      .CLKOUT4_CASCADE         ( "FALSE" ), // Cascade CLKOUT4 counter with CLKOUT6 (FALSE, TRUE)
      .DIVCLK_DIVIDE           ( 3 ),         // Master division value (1-106)
      .REF_JITTER1             ( 0.0 ),         // Reference input jitter in UI (0.000-0.999).
      .STARTUP_WAIT            ( "FALSE" )     // Delays DONE until MMCM is locked (FALSE, TRUE)
   )
   u_mmcm
   (
      .CLKOUT0                 ( clk_6p144m_pre ),     // 1-bit output: CLKOUT0
      .CLKOUT0B                ( ),   // 1-bit output: Inverted CLKOUT0
      .CLKFBOUT                ( clk_fb_pre ),   // 1-bit output: Feedback clock
      .CLKFBOUTB               ( ), // 1-bit output: Inverted CLKFBOUT
      .LOCKED                  ( ),       // 1-bit output: LOCK
      .CLKIN1                  ( clk_50m ),       // 1-bit input: Clock
      .PWRDWN                  ( 1'b0 ),       // 1-bit input: Power-down
      .RST                     ( 1'b0 ),             // 1-bit input: Reset
      // Feedback Clocks: 1-bit (each) input: Clock feedback ports
      .CLKFBIN                 ( clk_fb )      // 1-bit input: Feedback clock
   );
   
   BUFG u0_bufg
   (
      .I                       ( clk_fb_pre ),
      .O                       ( clk_fb )
   );
   
   BUFG u1_bufg
   (
      .I                       ( clk_6p144m_pre ),
      .O                       ( clk_6p144m )
   );
   
   always @(posedge clk_6p144m)
   begin
      clk_3p072m_pre <= ~clk_3p072m_pre;
   end
   
   BUFG u2_bufg
   (
      .I                       ( clk_3p072m_pre ),
      .O                       ( clk_3p072m )
   );
   
endmodule
