
module clock_meter
#(
   parameter                   TIME_1S = 100_000_000
)
(
   input                       clk_ref,
   input                       clk_tst,
   output     [31:0]           clk_rpt
);

   wire        time_1s;
   reg  [31:0] time_cnt = 0;
   wire        time_1s_tst;
   reg  [31:0] time_cnt_r;

   time_event
   #(
      .TIME_1S                 ( TIME_1S )
   )
   u_time_event
   (
      .clk                     ( clk_ref ),
      .time_1s                 ( time_1s )
   );
   
   sync_edge u_sync_edge
   (
      .iclk                    ( clk_ref ),
      .iedge                   ( time_1s ),
      .oclk                    ( clk_tst ),
      .oedge                   ( time_1s_tst )
   );
   
   always @(posedge clk_tst)
   begin
      if(time_1s_tst)
         time_cnt <= 0;
      else
         time_cnt <= time_cnt + ~&time_cnt;
   end
   
   always @(posedge clk_tst)
   begin
      if(time_1s_tst)
         time_cnt_r <= time_cnt;
   end
   
   sync_clock
   #(
      .DWID                    ( 32 )
   )
   u_sync_clock
   (
      .idata                   ( time_cnt_r ),
      .oclk                    ( clk_ref ),
      .odata                   ( clk_rpt )
   );
   
endmodule
