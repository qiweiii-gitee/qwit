
module time_event
#(
   parameter                   TIME_1S = 100_000_000
)
(
   input                       clk,
   output                      time_1s
);
   
   reg [31:0] time_cnt = 0;
   
   always @(posedge clk)
   begin
      if(time_cnt == TIME_1S - 1)
         time_cnt <= 0;
      else
         time_cnt <= time_cnt + ~&time_cnt;
   end
   
   assign time_1s = time_cnt == TIME_1S - 1;
   
endmodule
