
module sync_edge
(
   input                       iclk,
   input                       iedge,
   input                       oclk,
   output reg                  oedge
);
   
   reg      iedge_r;
   reg      oedge_tmp;
   reg      oedge_r;
   reg      iedge_tmp;
   reg      oedge_r1;
   
   always @(posedge iclk)
   begin
      if(iedge)
         iedge_r <= 1'b1;
      else if(oedge_tmp)
         iedge_r <= 1'b0;
   end
   
   always @(posedge iclk)
   begin
      oedge_tmp <= oedge_r;
   end
   
   always @(posedge oclk)
   begin
      if(iedge_tmp)
         oedge_r <= 1'b1;
      else
         oedge_r <= 1'b0;
   end
   
   always @(posedge oclk)
   begin
      iedge_tmp <= iedge_r;
   end
   
   always @(posedge oclk)
   begin
      oedge_r1 <= oedge_r;
      oedge <= ~oedge_r1 && oedge_r;
   end
   
endmodule
