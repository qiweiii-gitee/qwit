
module sdpram
#(
   parameter             DWID = 32,
   parameter             AWID = 10
)
(
   input                 wclk,
   input                 we,
   input      [AWID-1:0] waddr,
   input      [DWID-1:0] wdata,
   input                 rclk,
   input                 re,
   input      [AWID-1:0] raddr,
   output     [DWID-1:0] rdata
);
   
   reg [DWID-1:0] ram_int [0:2**AWID-1];
   reg [DWID-1:0] rdata_last;
   
   always @(posedge wclk)
   begin
      if(we)
         ram_int[waddr] <= wdata;
   end
   
   always @(posedge rclk)
   begin
      if(re)
         rdata_last <= rdata;
   end

   assign rdata = (re)? ram_int[raddr] : rdata_last;
   
endmodule
