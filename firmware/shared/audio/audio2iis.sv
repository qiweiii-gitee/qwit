
module audio2iis
(
   input                       clk_3p072m,
   input                       audio_data_valid,
   input      [31:0]           audio_data,
   output                      iis_sclk,
   output reg                  iis_lrck,
   output reg                  iis_data
);
   
   reg  [31:0]                 audio_data_r;
   reg  [5:0]                  audio_bit_cnt;
   
   always @(negedge clk_3p072m)
   begin
      if(audio_data_valid)
         audio_data_r <= audio_data;
   end
   
   always @(negedge clk_3p072m)
   begin
      if(audio_data_valid)
         audio_bit_cnt <= 63;
      else
         audio_bit_cnt <= audio_bit_cnt - |audio_bit_cnt;
   end
   
   always @(negedge clk_3p072m)
   begin
      if(audio_data_valid)
         iis_lrck <= 1'b0;
      else if(audio_bit_cnt == 32)
         iis_lrck <= 1'b1;
   end
   
   always @(negedge clk_3p072m)
   begin
      if(audio_bit_cnt > 47)
         iis_data <= audio_data_r[audio_bit_cnt-32];
      else if(audio_bit_cnt < 32 && audio_bit_cnt > 15)
         iis_data <= audio_data_r[audio_bit_cnt-16];
      else
         iis_data <= 1'b0;
   end
   
   assign iis_sclk = clk_3p072m;
   
endmodule
