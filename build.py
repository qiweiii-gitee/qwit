#*****************************************************************************
#    # #              Name   : build.py
#  #     #            Date   : Jun. 22, 2021
# #    #  #  #     #  Author : Qiwei Wu
#  #     #  # #  # #  Version: 1.0
#    # #  #    #   #
# build.py
#
# Change History:
#  VER.   Author         DATE              Change Description
#  1.0    Qiwei Wu       Jun. 22, 2021     Initial Release
#  1.1    Qiwei Wu       Jun. 25, 2021     Add code size detection for GetOrBuild firmware
#  1.2    Qiwei Wu       Jul. 02, 2021     Add s3c2440 minimum rootfs task to build
#  1.3    Qiwei Wu       Jul. 08, 2021     Get or build s3c2440 task
#  1.4    Qiwei Wu       Jul. 15, 2021     Build Zynq task
#  1.5    Qiwei Wu       Jul. 18, 2021     Add build option parser
#  2.0    Qiwei Wu       Apr. 29, 2021     New
#*****************************************************************************
#!/usr/bin/python

import os
import sys
import time
import shutil
from optparse import OptionParser

#*****************************************************************************
# Add persional python path
#*****************************************************************************
sys.path.append('./script')

# import FwTask will appears ERROR:TypeError: 'module' object is not callable
from FwBuild import *
from ZynqBuild import *
from GetOrBuild import *
from GetDepend import *
from ApplyPatch import *
import GetCodeSize
import DownloadFile

#*****************************************************************************
# Make folder
#*****************************************************************************
workPath = os.path.abspath( os.getcwd() ) + "/"
buildPath = workPath + ".build/"
dependPath = workPath + '.depend/'

parser = OptionParser()
parser.add_option( "-v", "--version", dest="ver", help="Version Number" )
( options, args ) = parser.parse_args()
verNum = options.ver[:7]

def Mkdirbuild():
   if os.path.exists( buildPath ):
      shutil.rmtree( buildPath )
      print( "%s is removed" % ( buildPath ) )
   os.makedirs( buildPath )
   print( "%s is created" % ( buildPath ) )
   if os.path.exists( dependPath ):
      shutil.rmtree( dependPath )
      print( "%s is removed" % ( dependPath ) )
   os.makedirs( dependPath )
   print( "%s is created" % ( dependPath ) )
   print( "Starting......")
   
#*****************************************************************************
# Firmware build
#*****************************************************************************
def FwBuilds():
   print('Firmware is building now')
   fwBuild = FwBuild(workPath + 'firmware/', buildPath, 'Platform')
   fwBuild.runBuild('platform')
   print('Firmware finished building')

#*****************************************************************************
# Zynq task
#*****************************************************************************
def ZynqSwTask():
   zynqBuild.runBootbin(buildPath)
   zynqBuild.runSw('qwitss')
   zynqBuild.runSw('regrw')
   zynqBuild.runSw('server.cgi')
   zynqBuild.runRootfs(workPath + 'script/depend/', verNum)
   zynqBuild.buildImage(verNum)
   print('ZYNQ software finished building')
   
#*****************************************************************************
# Build Start
#*****************************************************************************
if __name__ == "__main__":
   pathNow = os.path.abspath( os.getcwd() ) + "/"
   Mkdirbuild()

   # Copy depend
   os.system( 'cp -rf /root/git/depend %s' %(workPath + "script/"))
   
   # Build Firmware
   getOrBuild = GetOrBuild(workPath, buildPath, 'CodeSize')
   buildFw = getOrBuild.GetOrBuildFw(buildPath + 'Platform')
   if buildFw == 1:
      FwBuilds()
   # Build Boot
   os.chdir( workPath + 'software/boot' )
   os.system( 'make new' )
   # Build Kernel
   os.chdir( workPath + 'software/kernel' )
   os.system( 'make new' )
   # Build Rootfs
   os.chdir( workPath + 'software/rootfs' )
   os.system( 'make new' )
   # Build Image
   os.chdir( workPath + 'software/image' )
   os.system( 'make new' )