#*****************************************************************************
#    # #              Name   : FwBuild.py
#  #     #            Date   : Jun. 25, 2021
# #    #  #  #     #  Author : Qiwei Wu
#  #     #  # #  # #  Version: 1.0
#    # #  #    #   #
# FwBuild.py
#
# Change History:
#  VER.   Author         DATE              Change Description
#  1.0    Qiwei Wu       Jun. 25, 2021     Initial Release
#*****************************************************************************
#!/usr/bin/python

import os
import sys
import time
import shutil

#*****************************************************************************
# Add persional python path
#*****************************************************************************
from Artifacts import *

#*****************************************************************************
# Class
#*****************************************************************************
class FwBuild:

   def __init__(self, workPath, buildPath, name=''):
      if name == '':
         print( "No path name found" )
         sys.exit( -1 )
      self.workPath = workPath
      self.buildPath = buildPath + '/' + name + '_prebuild/'
      self.resPath = buildPath + '/' + name + '/'

   def runBuild(self, name=''):
      if name == '':
         print( "No task name found" )
         sys.exit( -1 )
      print( "Task %s is building now" % ( name ) )
      pathNow = os.path.abspath( os.getcwd() ) + "/"
      if os.path.exists( self.buildPath ):
         shutil.rmtree( self.buildPath )
      os.makedirs( self.buildPath )
      # Copy folder tree
      os.system( 'cp -r %s/* %s' % (self.workPath, self.buildPath) )
      # Build firmware
      os.chdir( self.buildPath + name + '/build' )
      # Modify run scripts
      with open('Run.tcl', 'a') as file:
         file.write( 'RunFw %s xc7z020clg400-2 0 2018 \r\n' % ( name ) )
      file.close
      with open('RunSw.tcl', 'a') as file:
         file.write( 'RunFsbl %s \r\n' % ( name ) )
      file.close
      # Start building
      #os.system( '/tools/Xilinx/Vivado/2019.2/bin/vivado -mode batch -source Run.tcl' )
      os.system( '/opt/Xilinx/Vivado/2018.1/bin/vivado -mode batch -source Run.tcl' )
      os.system( 'cp RunSw.tcl %s' %( self.buildPath + name + '/build/' + name + '.sdk/' ))
      os.chdir( self.buildPath + name + '/build/' + name + '.sdk/' )
      os.system( '/opt/Xilinx/SDK/2018.1/bin/xsdk -batch -source RunSw.tcl' )
      # Publish artifaces
      artifacts = Artifacts( self.resPath, '/' )
      artifacts.publish( self.buildPath + name + '/build/' + name + '.runs/impl_1/', name + '.bit')
      artifacts.publish( self.buildPath + name + '/build/' + name + '.runs/impl_1/', name + '_timing_summary_routed.rpt')
      artifacts.publish( self.buildPath + name + '/build/' + name + '.runs/impl_1/', name + '_utilization_placed.rpt')
      artifacts.publish( self.buildPath + name + '/build/' + name + '.sdk/', name + '.xsa')
      artifacts.publish( self.buildPath + name + '/build/' + name + '.sdk/', name + '.hdf')
      artifacts.publish( self.buildPath + name + '/build/' + name + '.sdk/sw_workspace/' + name + '_fsbl/Debug/', name + '_fsbl.elf')
      # Remove build path
      os.chdir( pathNow )
      time.sleep( 10 )
      shutil.rmtree( self.buildPath )
      print( "Task %s build finished" % name )
