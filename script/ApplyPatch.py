#*****************************************************************************
#    # #              Name   : ApplyPatch.py
#  #     #            Date   : Jul. 01, 2021
# #    #  #  #     #  Author : Qiwei Wu
#  #     #  # #  # #  Version: 1.0
#    # #  #    #   #
# ApplyPatch.py
#
# Change History:
#  VER.   Author         DATE              Change Description
#  1.0    Qiwei Wu       Jul. 01, 2021     Initial Release
#*****************************************************************************
#!/usr/bin/python

import os
import shutil

#*****************************************************************************
# Class
#*****************************************************************************
class ApplyPatch:
   def __init__(self, resPath=''):
      if resPath == '':
         print( "No path name found" )
         sys.exit( -1 )
      self.resPath = resPath + '/'

   def apply(self, name='',patch=''):
      if patch == '':
         print( "No patch name found" )
         sys.exit( -1 )
      pathNow = os.path.abspath( os.getcwd() ) + "/"
      print('Applying patch %s into %s' %(patch, self.resPath + name))
      os.chdir( self.resPath + name )
      os.system('patch -p1 < %s' % patch)
      os.chdir( pathNow )
