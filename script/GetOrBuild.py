#*****************************************************************************
#    # #              Name   : GetOrBuild.py
#  #     #            Date   : Jun. 26, 2021
# #    #  #  #     #  Author : Qiwei Wu
#  #     #  # #  # #  Version: 1.0
#    # #  #    #   #
# GetOrBuild.py
#
# Change History:
#  VER.   Author         DATE              Change Description
#  1.0    Qiwei Wu       Jun. 26, 2021     Initial Release
#  1.1    Qiwei Wu       Jul. 08, 2021     Get or build s3c2440 tasks
#  1.2    Qiwei Wu       Jul. 15, 2021     Get or build Zynq Uboot and Kernel tasks
#*****************************************************************************
#!/usr/bin/python

import os

#*****************************************************************************
# Add persional python path
#*****************************************************************************
from Artifacts import *
import GetCodeSize
import DownloadFile

#*****************************************************************************
# Class
#*****************************************************************************
class GetOrBuild:
   def __init__(self, workPath, resPath, name=''):
      if name == '':
         print( "No path name found" )
         sys.exit( -1 )
      self.workPath = workPath
      self.resPath = resPath + '/' + name
      self.artifacts = Artifacts( self.resPath, '/' )

   #*****************************************************************************
   # Get or build firmware
   #*****************************************************************************
   def GetOrBuildFw(self, resPath=''):
      build = 1
      if resPath == '':
         print( "No path name found" )
         sys.exit( -1 )

      codeSize = GetCodeSize.getFwCodeSize( self.workPath + 'firmware/' )

      with open( 'codeSizeFw.txt', 'w' ) as outFile:
         outFile.write('%d\r\n' % codeSize)
      outFile.close
      self.artifacts.publish( './', 'codeSizeFw.txt' )

      if os.path.exists( 'codeSizeFw.txt' ):
         os.remove( 'codeSizeFw.txt' )
      url = 'http://8.142.31.205:8111/repository/download/Qwit_Master/.lastSuccessful/'
      url = url + 'CodeSize/codeSizeFw.txt'
      get = DownloadFile.downloadArtifact(url, 'codeSizeFw.txt')
      if get == 0 :
         return build

      inFile = open( 'codeSizeFw.txt', 'r' )
      for line in inFile.readlines():
         lastCodeSize = line
      inFile.close
      os.remove('codeSizeFw.txt')

      print('Last code size is %d and now is %d' % (int(lastCodeSize), codeSize))

      if int(lastCodeSize) == codeSize:
         build = 0
         # Download firmwares
         print('Downloading the firmware files now')
         url = 'http://8.142.31.205:8111/repository/download/Qwit_Master/.lastSuccessful/'
         url = url + 'Platform/platform.bit'
         get = DownloadFile.downloadArtifact(url, 'platform.bit')
         if get == 0 :
            build = 1
         url = 'http://8.142.31.205:8111/repository/download/Qwit_Master/.lastSuccessful/'
         url = url + 'Platform/platform_timing_summary_routed.rpt'
         get = DownloadFile.downloadArtifact(url, 'platform_timing_summary_routed.rpt')
         if get == 0 :
            build = 1
         url = 'http://8.142.31.205:8111/repository/download/Qwit_Master/.lastSuccessful/'
         url = url + 'Platform/platform_utilization_placed.rpt'
         get = DownloadFile.downloadArtifact(url, 'platform_utilization_placed.rpt')
         if get == 0 :
            build = 1
         url = 'http://8.142.31.205:8111/repository/download/Qwit_Master/.lastSuccessful/'
         url = url + 'Platform/platform.hdf'
         get = DownloadFile.downloadArtifact(url, 'platform.hdf')
         if get == 0 :
            build = 1
         url = 'http://8.142.31.205:8111/repository/download/Qwit_Master/.lastSuccessful/'
         url = url + 'Platform/platform_fsbl.elf'
         get = DownloadFile.downloadArtifact(url, 'platform_fsbl.elf')
         if get == 0 :
            build = 1
         # Publishing firmwares
         if build == 0:
            print('Finished Downloading the firmware files, publishing now')
            artifacts = Artifacts( resPath, '/' )
            artifacts.publish( './', 'platform.bit' )
            #artifacts.publish( './', 'platform.xsa' )
            artifacts.publish( './', 'platform.hdf' )
            artifacts.publish( './', 'platform_timing_summary_routed.rpt' )
            artifacts.publish( './', 'platform_utilization_placed.rpt' )
            artifacts.publish( './', 'platform_fsbl.elf' )

      return build

   #*****************************************************************************
   # Get or build Zynq Uboot tasks
   #*****************************************************************************
   def GetOrBuildZynqUboot(self, resPath=''):
      build = 1
      if resPath == '':
         print( "No path name found" )
         sys.exit( -1 )

      codeSize = GetCodeSize.getCodeSize( self.workPath + 'script/patch' )
      with open( 'codeSizeZynqUboot.txt', 'w' ) as outFile:
         outFile.write('%d\r\n' % codeSize)
      outFile.close
      self.artifacts.publish( './', 'codeSizeZynqUboot.txt' )
      
      if os.path.exists( 'codeSizeZynqUboot.txt' ):
         os.remove( 'codeSizeZynqUboot.txt' )
      url = 'http://8.142.31.205:8111/repository/download/Qwit_Master/.lastSuccessful/'
      url = url + 'CodeSize/codeSizeZynqUboot.txt'
      get = DownloadFile.downloadArtifact(url, 'codeSizeZynqUboot.txt')
      if get == 0 :
         return build
      
      inFile = open( 'codeSizeZynqUboot.txt', 'r' )
      for line in inFile.readlines():
         lastCodeSize = line
      inFile.close
      os.remove('codeSizeZynqUboot.txt')
      
      print('Last code size is %d and now is %d' % (int(lastCodeSize), codeSize))
      
      if int(lastCodeSize) == codeSize:
         build = 0
         # Download files
         print('Downloading the Zynq Uboot task files now')
         url = 'http://8.142.31.205:8111/repository/download/Qwit_Master/.lastSuccessful/'
         url = url + 'Zynq/u-boot/u-boot'
         get = DownloadFile.downloadArtifact(url, 'u-boot')
         if get == 0 :
            build = 1
         if build == 0:
            print('Finished Downloading the Zynq Uboot files, publishing now')
            artifacts = Artifacts( resPath, '/' )
            artifacts.publish( './', 'u-boot' )
            
      return build
            
   #*****************************************************************************
   # Get or build Zynq Kernel tasks
   #*****************************************************************************
   def GetOrBuildZynqKernel(self, resPath=''):
      build = 1
      if resPath == '':
         print( "No path name found" )
         sys.exit( -1 )

      codeSize = GetCodeSize.getCodeSize( self.workPath + 'script/patch' )
      codeSize = GetCodeSize.getCodeSize( self.workPath + 'software/zynq/configuration' ) + codeSize
      with open( 'codeSizeZynqKernel.txt', 'w' ) as outFile:
         outFile.write('%d\r\n' % codeSize)
      outFile.close
      self.artifacts.publish( './', 'codeSizeZynqKernel.txt' )
      
      if os.path.exists( 'codeSizeZynqKernel.txt' ):
         os.remove( 'codeSizeZynqKernel.txt' )
      url = 'http://8.142.31.205:8111/repository/download/Qwit_Master/.lastSuccessful/'
      url = url + 'CodeSize/codeSizeZynqKernel.txt'
      get = DownloadFile.downloadArtifact(url, 'codeSizeZynqKernel.txt')
      if get == 0 :
         return build
      
      inFile = open( 'codeSizeZynqKernel.txt', 'r' )
      for line in inFile.readlines():
         lastCodeSize = line
      inFile.close
      os.remove('codeSizeZynqKernel.txt')
      
      print('Last code size is %d and now is %d' % (int(lastCodeSize), codeSize))
      
      if int(lastCodeSize) == codeSize:
         build = 0
         # Download files
         print('Downloading the Zynq Kernel task files now')
         url = 'http://8.142.31.205:8111/repository/download/Qwit_Master/.lastSuccessful/'
         url = url + 'Zynq/kernel/uImage'
         get = DownloadFile.downloadArtifact(url, 'uImage')
         if get == 0 :
            build = 1
         url = 'http://8.142.31.205:8111/repository/download/Qwit_Master/.lastSuccessful/'
         url = url + 'Zynq/kernel/devicetree.dtb'
         get = DownloadFile.downloadArtifact(url, 'devicetree.dtb')
         if get == 0 :
            build = 1
         if build == 0:
            print('Finished Downloading the Zynq Kernel files, publishing now')
            artifacts = Artifacts( resPath, '/' )
            artifacts.publish( './', 'uImage' )
            artifacts.publish( './', 'devicetree.dtb' )
            
      return build
