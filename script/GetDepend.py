#*****************************************************************************
#    # #              Name   : GetDepend.py
#  #     #            Date   : Jun. 26, 2021
# #    #  #  #     #  Author : Qiwei Wu
#  #     #  # #  # #  Version: 1.0
#    # #  #    #   #
# GetOrBuild.py
#
# Change History:
#  VER.   Author         DATE              Change Description
#  1.0    Qiwei Wu       Jun. 26, 2021     Initial Release
#*****************************************************************************
#!/usr/bin/python

import os
import shutil

#*****************************************************************************
# Add persional python path
#*****************************************************************************
import DownloadFile

#*****************************************************************************
# Class
#*****************************************************************************
class GetDepend:
   def __init__(self, resPath=''):
      if resPath == '':
         print( "No path name found" )
         sys.exit( -1 )
      self.resPath = resPath + '/'
      if os.path.exists( self.resPath ):
         shutil.rmtree( self.resPath )
      os.makedirs( self.resPath )

   def getFromLocal(self, srcPath, file=''):
      if file == '':
         print( "No file name found" )
         sys.exit( -1 )
      fileSrcPath = os.path.join( srcPath, file )
      fileDestPath = os.path.join( self.resPath, file )
      print('Copying the %s from %s to %s' %(file, srcPath, self.resPath))
      shutil.copy( fileSrcPath, fileDestPath )
      print('Finished Copying')
      if '.tar.bz2' in file:
         os.system('tar -xjf %s -C %s' % ( fileDestPath, self.resPath))
      if '.tar.gz' in file:
         os.system('tar -zxvf %s -C %s' % ( fileDestPath, self.resPath))
      if '.zip' in file:
         os.system('unzip -o %s -d %s' % ( fileDestPath, self.resPath))
      print('Finished unzipping')

   def getFromGit(self, url='', file=''):
      if url == '':
         print( "No url name found" )
         sys.exit( -1 )
      fileDestPath = os.path.join( self.resPath, file )
      DownloadFile.gitClone(url, fileDestPath)
      print('Finished getting from %s' % url)
