#*****************************************************************************
#    # #              Name   : GetCodeSize.py
#  #     #            Date   : Jun. 23, 2021
# #    #  #  #     #  Author : Qiwei Wu
#  #     #  # #  # #  Version: 1.0
#    # #  #    #   #
# getFwCodeSize.py
#
# Change History:
#  VER.   Author         DATE              Change Description
#  1.0    Qiwei Wu       Jun. 23, 2021     Initial Release
#*****************************************************************************
#!/usr/bin/python
import os
 
def getFwCodeSize(folderPath):
   totalSize = 0

   if not os.path.exists(folderPath):
      return totalSize
  
   if os.path.isfile(folderPath):
      totalSize = os.path.getsize(folderPath)
      return totalSize

   for root, dirs, files in os.walk(folderPath):
      for name in files:
         if '.v' in name or '.vh' in name or '.sv' in name or '.tcl' in name:
            # Remove the .ipynb_checkpoint folder affect
            if not '.ipynb' in root:
               size = os.path.getsize(os.path.join(root, name))
               totalSize += size

   print('The total size of %s is %d' %( folderPath, totalSize ) )

   return totalSize

def getCodeSize(folderPath):
   totalSize = 0

   if not os.path.exists(folderPath):
      return totalSize
  
   if os.path.isfile(folderPath):
      totalSize = os.path.getsize(folderPath)
      return totalSize

   for root, dirs, files in os.walk(folderPath):
      for name in files:
         #if '.v' in name or '.vh' in name or '.sv' in name or '.tcl' in name:
         # Remove the .ipynb_checkpoint folder affect
         if not '.ipynb' in root:
            size = os.path.getsize(os.path.join(root, name))
            totalSize += size

   print('The total size of %s is %d' %( folderPath, totalSize ) )

   return totalSize
