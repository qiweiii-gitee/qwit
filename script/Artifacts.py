#*****************************************************************************
#    # #              Name   : Artifaces.py
#  #     #            Date   : Jun. 23, 2021
# #    #  #  #     #  Author : Qiwei Wu
#  #     #  # #  # #  Version: 1.0
#    # #  #    #   #
# taskRun.py
#
# For download files from teamcity
##http://8.142.31.205:8111/repository/download/Qwit_Master/.lastSuccessful/.build/FwTasks/codes/result.log
##http://8.142.31.205:8111/repository/downloadAll/Qwit_Master/08eb98400daa2909434bfe154a6eff9bcee81715/artifacts.zip
##http://8.142.31.205:8111/repository/download/Qwit_Master/08eb98400daa2909434bfe154a6eff9bcee81715/.build/FwTasks/codes/result.log
#
#
# Change History:
#  VER.   Author         DATE              Change Description
#  1.0    Qiwei Wu       Jun. 23, 2021     Initial Release
#*****************************************************************************
#!/usr/bin/python

import os
import sys
import time
import shutil

class Artifacts:

   def __init__(self, path, name=''):
      if name == '':
         print( "No path name found" )
         sys.exit( -1 )
      self.path = path + '/' + name
      #if os.path.exists( self.path ):
      #   shutil.rmtree( self.path )
      if not os.path.exists( self.path ):
         os.makedirs( self.path )
      
   def publish(self, srcPath, file=''):
      if file == '':
         print( "No file name found" )
         sys.exit( -1 )
      fileSrcPath = os.path.join( srcPath, file )
      fileDestPath = os.path.join( self.path, file )
      if not os.path.exists( fileSrcPath ):
         print('no %s found in %s' % (file, srcPath))
         return 0
      shutil.copy( fileSrcPath, fileDestPath )
      os.remove(fileSrcPath)
