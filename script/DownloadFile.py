#*****************************************************************************
#    # #              Name   : DownloadFile.py
#  #     #            Date   : Jun. 25, 2021
# #    #  #  #     #  Author : Qiwei Wu
#  #     #  # #  # #  Version: 1.0
#    # #  #    #   #
# DownloadFile.py
#
# Change History:
#  VER.   Author         DATE              Change Description
#  1.0    Qiwei Wu       Jun. 25, 2021     Initial Release
#*****************************************************************************
#!/usr/bin/python

import os
 
def downloadArtifact(url, file):
   print('Download %s from %s' %(file, url))
   os.system('wget --http-user=qiweiw --http-passwd=123456 %s' % url)
   if not os.path.exists( file ):
      return 0
   return 1

def gitClone(url, resPath):
   print('Clone %s from %s' %(resPath, url))
   os.system( 'git clone %s %s' % (url, resPath) )
   if not os.path.exists( resPath ):
      return 0
   return 1
