
//*****************************************************************************
// Headers
//*****************************************************************************
#include <sys/mman.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/msg.h>
#include <errno.h>
#include <stdbool.h>
#include <cgic.h>

//*****************************************************************************
// Defines
//*****************************************************************************
#define BufferLen 1024
#define MAXMSG 2048

char *plays[] = {
	"Play",
	"Stop"
};

//*****************************************************************************
// MsgQ
//*****************************************************************************
typedef struct _BOX  
{  
    long type;  
    char szMsg[MAXMSG];  
}Box;

int updatePlatformMsg()
{
   int msgid = msgget((key_t)1024, 0666 | IPC_CREAT);
   Box box;
   box.type = 1;
   
   snprintf(box.szMsg, sizeof(box.szMsg), "Update System Now.");
   msgsnd(msgid, (void*)&box, sizeof(box.szMsg), 0);
   
   return 0;
}

int playMusicMsg( bool play )
{
   int msgid = msgget((key_t)1024, 0666 | IPC_CREAT);
   Box box;
   box.type = 1;
   
   if( play == true )
   {
      snprintf(box.szMsg, sizeof(box.szMsg), "Play Music. 0");
   }
   else
   {
      snprintf(box.szMsg, sizeof(box.szMsg), "Stop Music. 0");
   }
   
   msgsnd(msgid, (void*)&box, sizeof(box.szMsg), 0);
   
   return 0;
}

//*****************************************************************************
// Get file from server
//*****************************************************************************
int getFileFromServer( bool audioFile )
{
   cgiFilePtr file;
   int targetFile;
   mode_t mode;
   char name[128];
   char fileNameOnServer[64];
   char contentType[1024];
   char buffer[BufferLen];
   char *tmpStr=NULL;
   int size;
   int got;
   
   // 取得html页面中file元素的值，应该是文件在客户机上的路径名
   if(cgiFormFileName("file", name, sizeof(name)) != cgiFormSuccess)
   {
      printf("could not get filename. \r\n");
      goto FAIL;
   }

   cgiFormFileSize("file", &size);
   
   // 取得文件类型
   cgiFormFileContentType("file", contentType, sizeof(contentType));
   
   // 打开文件
   if (cgiFormFileOpen("file", &file) != cgiFormSuccess)
   {
      printf("could not open the file. \r\n");
      goto FAIL;
   }
   
   // 判断是否是升级文件
   if(strstr(name, "upgrade"))
   {
      strcpy(fileNameOnServer, "/tmp/upgrade");
   }
   // 判断是否是音频文件
   else if(audioFile == true)
   {
      strcpy(fileNameOnServer, "/tmp/m0.aud");
   }
   else
   {
      strcpy(fileNameOnServer, "/tmp/");
   }
   
   mode = S_IRWXU|S_IRGRP|S_IROTH;
   
   // 在fileNameOnServer目录下建立新的文件
   targetFile = open(fileNameOnServer, O_RDWR|O_CREAT|O_TRUNC|O_APPEND, mode);
   if(targetFile < 0)
   {
      printf("could not create the file %s. \r\n", fileNameOnServer);
      goto FAIL;
   }
   
   // 从系统临时文件中读出文件内容，并放到刚创建的目标文件中
   while(cgiFormFileRead(file, buffer, BufferLen, &got) == cgiFormSuccess)
   {
      if(got > 0)
         write(targetFile, buffer, got);
   }

   cgiFormFileClose(file);
   close(targetFile);
   goto END;

   FAIL:
      printf("Failed to upload \r\n");
      return 1;
   END:
      printf("File %s has been uploaded", fileNameOnServer);
      return 0;
}

//*****************************************************************************
// Main
//*****************************************************************************
int cgiMain(int argc, char **argv)
{
   int ret;
   int len;
   int playChoice;
   char *postStr, inputData[512];

   // Get the value from FORM - no CGIC method
   /*
   postStr = getenv("CONTENT_LENGTH");
   if(postStr != NULL)
   {
      len = atoi(postStr);
      fgets(inputData, len + 2, stdin);
   }
   */

   if(cgiFormString("updatePlatform", inputData, 512) != cgiFormNotFound)
   {
      ret = getFileFromServer( false );
      if(ret == 0)
      {
         updatePlatformMsg();
      }
   }
   else if(cgiFormString("playMusic", inputData, 512) != cgiFormNotFound)
   {
      cgiFormRadio("play", plays, 2, &playChoice, 0);
      if(strstr(plays[playChoice], "Play"))
      {
         ret = getFileFromServer( true );
         if(ret == 0)
         {
            playMusicMsg( true );
         }
         else
         {
            playMusicMsg( false );
         }
      }
      else if(strstr(plays[playChoice], "Stop"))
      {
         playMusicMsg( false );
      }
   }

   return 0;
}
