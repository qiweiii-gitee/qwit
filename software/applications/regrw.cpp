//*****************************************************************************
//    # #              Name   : regrw.c
//  #     #            Date   : Feb. 12, 2021
// #    #  #  #     #  Author : Qiwei Wu
//  #     #  # #  # #  Version: 2.0
//    # #  #    #   #
//
// This module is the register read/write control.
// 
// Change History:
//  VER.   Author         DATE              Change Description
//  1.0    Qiwei Wu       Dec. 23, 2020     Initial Release
//  2.0    Qiwei Wu       Feb. 12, 2021     New codes
//*****************************************************************************

//*****************************************************************************
// Headers
//*****************************************************************************
#include <sys/mman.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <StrToInt.h>
#include <Reg.h>

//*****************************************************************************
// Functions
//*****************************************************************************
int main(int argc, char **argv)
{
   unsigned int  address;
   unsigned int  rData;
   unsigned int  wData;

   Reg m_Reg( 0x41000000 );
   StrToInt m_StrToInt;

   if (argc != 2 && argc != 3)
   {
      printf("Usage :\n");
      printf("%s address <write value>\n", argv[0]);
      return 0;
   }

   // Read
   if(argc == 2)
   {
      address = m_StrToInt.StrToIntCtrl(argv[1], strlen(argv[1]));
      rData = m_Reg.Read(address);
      printf("Reading 0x%x from 0x%x\n", rData, address);
   }
   // Write
   else if(argc == 3)
   {
      address = m_StrToInt.StrToIntCtrl(argv[1], strlen(argv[1]));
      wData = m_StrToInt.StrToIntCtrl(argv[2], strlen(argv[2]));
      m_Reg.Write(address, wData);
      printf("Writing 0x%x to 0x%x\n", wData, address);
   }

   return 0;
}
