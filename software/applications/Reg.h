
#ifndef __REG_H
#define __REG_H

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <iostream>
#include <fcntl.h>
#include <sys/mman.h>
#include <linux/ioctl.h>

class Reg
{
public:
   Reg( unsigned int baseAddress );
   virtual ~Reg();

   void Write( unsigned int address, unsigned int data );
   unsigned int Read( unsigned int address );
   
private:
   void RegMap( unsigned int baseAddress );
   void RegUnMap();
   
   void *m_MapAddress;
};

#endif