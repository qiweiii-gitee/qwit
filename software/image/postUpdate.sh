#*****************************************************************************
#    # #              Name   : postUpdate.sh
#  #     #            Date   : Jul. 22, 2021
# #    #  #  #     #  Author : Qiwei Wu
#  #     #  # #  # #  Version: 1.0
#    # #  #    #   #
# postUpdate.sh.
# 
# Change History:
#  VER.   Author         DATE              Change Description
#  1.0    Qiwei Wu       Jul. 22, 2021     Initial Release
#*****************************************************************************
#!/bin/sh

if [ -f /tmp/upgrade ]
then
   echo "Updatting"
   rm -f /mnt/*
   tar -xvf /tmp/upgrade -C /mnt
   mv /mnt/image/* /mnt
   echo "rebooting"
   reboot
elif [ -f /tmp/platform_123456.tar.gz ]
then
   cp -f /tmp/platform_123456.tar.gz /tmp/upgrade
   rm -f /mnt/*
   tar -xvf /tmp/upgrade -C /mnt
   echo "rebooting"
   reboot
else
   echo "no update software file"
fi
